module Diff where

import Prelude

import Data.Array as Array
import Data.Function.Memoize (memoize2)
import Data.List (List(..), intercalate, (:))
import Data.List as List
import Data.Maybe (Maybe(..))
import Data.Unfoldable (replicate)
import Partial.Unsafe (unsafePartial)


data Edit a
  = Insert a
  | Replace a
  | Match a
  | Delete

derive instance functorEdit :: Functor Edit

printEdit :: forall a. (a -> String) -> Edit a -> String
printEdit printItem = case _ of
  Insert x -> "Insert " <> printItem x
  Replace x -> "Replace " <> printItem x
  Match x -> "Match " <> printItem x
  Delete -> "Delete"

printEdits :: forall a. (a -> String) -> List (Edit a) -> String
printEdits printItem = intercalate " - " <<< map (printEdit printItem)

printEditHistory :: forall a. (a -> String) -> EditHistory a -> String
printEditHistory printItem (EditHistory h) = show $ h
  { edits = printEdits printItem h.edits
  }

newtype EditHistory a = EditHistory
  { edits :: List (Edit a)
  , cost :: Int
  }

instance Semigroup (EditHistory a) where
  append :: EditHistory a -> EditHistory a -> EditHistory a
  append h1 h2 = EditHistory
    { edits: edits h1 <> edits h2
    , cost : cost h1 + cost h2
    }

edits :: forall a. EditHistory a -> List (Edit a)
edits (EditHistory h) = h.edits

cost :: forall a. EditHistory a -> Int
cost (EditHistory h) = h.cost

match :: forall a. a -> EditHistory a
match x = EditHistory
  { edits: List.singleton $ Match x
  , cost: 0
  }

replace :: forall a. a -> EditHistory a
replace x = EditHistory
  { edits: List.singleton $ Replace x
  , cost: 1
  }

delete :: forall a. EditHistory a
delete = EditHistory
  { edits: List.singleton Delete
  , cost: 1
  }

insert :: forall a. a -> EditHistory a
insert x = EditHistory
  { edits: List.singleton $ Insert x
  , cost: 1
  }

diff :: forall a. (a -> a -> Boolean) -> Array a -> Array a -> List (Edit a)
diff checkMatch items1 items2 = List.reverse $ edits $ getEdits (Array.length items1 - 1) (Array.length items2 - 1)

  where
    getEdits :: Int -> Int -> EditHistory a
    getEdits = memoize2 getEditsSlow

      where
        getEditsSlow :: Int -> Int -> EditHistory a
        getEditsSlow i j =
          if i < 0 then
            insertFirst j
          else if j < 0 then
            deleteFirst (i + 1)
          else
            lowestCost
              ( subOrMatch i j <> getEdits (i - 1) (j - 1) )
              ( delete <> getEdits (i - 1) j )
              ( insert (getItemAtJ j) <> getEdits i (j - 1) )

    getItemAtI :: Int -> a
    getItemAtI i = unsafePartial (Array.unsafeIndex items1 i)

    getItemAtJ :: Int -> a
    getItemAtJ j = unsafePartial (Array.unsafeIndex items2 j)

    isMatch :: Int -> Int -> Boolean
    isMatch = memoize2 \i j ->
      let
        itemAtI = getItemAtI i
        itemAtJ = getItemAtJ j
      in checkMatch itemAtI itemAtJ

    insertFirst :: Int -> EditHistory a
    insertFirst j =
      if j < 0 then
        EditHistory { edits: Nil, cost: 0 }
      else
        insert (getItemAtJ j) <> insertFirst (j - 1)

    deleteFirst :: Int -> EditHistory a
    deleteFirst i =
      EditHistory
        { edits: replicate i Delete
        , cost: i
        }

    lowestCost :: EditHistory a -> EditHistory a -> EditHistory a -> EditHistory a
    lowestCost h1 h2 h3 =
      if cost h1 <= cost h2 then
        if cost h1 <= cost h3 then
          h1
        else
          h3
      else if cost h2 <= cost h3 then
        h2
      else
        h3

    subOrMatch :: Int -> Int -> EditHistory a
    subOrMatch i j =
      if isMatch i j then
        match (getItemAtJ j)
      else
        replace (getItemAtJ j)

reconstruct :: forall a. Array a -> List (Edit a) -> Maybe (Array a)
reconstruct initial = go List.Nil (List.fromFoldable initial) >>> map (List.reverse >>> Array.fromFoldable)

  where

    go :: List a -> List a -> List (Edit a) -> Maybe (List a)
    go acc orig editList =
      case orig of
        List.Nil ->
          applyRemainingInserts acc editList
        _ : restOrig ->
          case editList of
            List.Nil ->
              Nothing
            edit : restEdits ->
              let
                acc' = case edit of
                  Match y -> y : acc
                  Replace y -> y : acc
                  Insert y -> y : acc
                  Delete -> acc
                restOrig' = case edit of
                  Insert _ -> orig
                  _ -> restOrig
              in
                go acc' restOrig' restEdits

    applyRemainingInserts :: List a -> List (Edit a) -> Maybe (List a)
    applyRemainingInserts acc = case _ of
      List.Nil -> Just acc
      (Insert x) : rest -> applyRemainingInserts (x : acc) rest
      _ -> Nothing
